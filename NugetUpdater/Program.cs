﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml.Linq;

namespace NugetUpdater
{
    class Program
    {
        static void Main(string[] args)
        {
            var preview = false;

            // Repository ///////////////////////
            
            var rootFolder = new DirectoryInfo(@"c:\projects\");
            //var repositoryFolder = new DirectoryInfo(Path.Combine( rootFolder.FullName, @"tov"));
            var repositoryFolder = new DirectoryInfo(Path.Combine(rootFolder.FullName, @"aline.ordermanager"));
            //var repositoryFolder = new DirectoryInfo(Path.Combine(rootFolder.FullName, @"evita.operationcore"));

            
            // OLD TeamCity Build  ///////////////////////
            var oldSprint = 56;
            
            var oldnugetPackageWasBeta = false;  //var oldNugetPackageVersion = $"1.0.{oldSprint}.1";
            //var oldnugetPackageWasBeta = true; //var oldNugetPackageVersion = $"1.0.{oldSprint}.{oldTCBuild}-beta";

            var oldTCBuilds = new[] {@"5413"}; // only relevant if oldnugetPackageWasBeta == true
            
            
            // NEW TeamCity Build  ///////////////////////
            var newSprint = 57;
            var newTCBuild = @"5627";
            

            // Rest should not change //////////////////////////////
            var newNugetPackageVersion = $"1.0.{newSprint}.{newTCBuild}-beta";
            var xpathHintPath = XName.Get("HintPath", "http://schemas.microsoft.com/developer/msbuild/2003");

            foreach (var oldTCBuild in oldTCBuilds)
            {
                var oldNugetPackageVersion = 
                    oldnugetPackageWasBeta ? $"1.0.{oldSprint}.{oldTCBuild}-beta" : $"1.0.{oldSprint}.1";

                Console.WriteLine($"Looking for nuget package {oldNugetPackageVersion} in packages.config files ...");
                
                foreach (var packageFile in repositoryFolder.EnumerateFiles("packages.config",SearchOption.AllDirectories))
                {
                    var packageConfigDocument = XDocument.Load(packageFile.FullName);
                    var packages = packageConfigDocument.Descendants("package")
                        .Where(p => p.Attribute("version")?.Value == oldNugetPackageVersion);

                    if (packages.Any())
                    {
                        Console.WriteLine(
                            $"{GetRelativePath(packageFile, repositoryFolder), -60}");
                        if (!preview)
                        {
                            packageConfigDocument.Save(packageFile.FullName + ".orig");
                        }
                    }

                    foreach (var package in packages)
                    {
                        Console.WriteLine(
                            $"\t{package.Attribute("id").Value,-60}{package.Attribute("version").Value, -20} => {newNugetPackageVersion}");
                        package.Attribute("version").Value = newNugetPackageVersion;
                    }

                    if (!preview)
                    {
                        packageConfigDocument.Save(packageFile.FullName);
                    }
                }

                Console.WriteLine(new string('=', 80));
                var zero = "0";
                var replacements = new[]
                {
                    new {assembly = "", 
                        oldAssemblyVersion = oldnugetPackageWasBeta 
                        ? $"1.0.{oldSprint - 1}.{oldTCBuild}" 
                        : $"1.0.{oldSprint}.1", 
                        newAssemblyVersion = $"1.0.{newSprint - 1}.{newTCBuild}"},
                    new {assembly = "Agidens.Terminal.Suite.Service.Utility", 
                        oldAssemblyVersion = $"1.0.48.2", 
                        newAssemblyVersion = $"1.0.48.{newTCBuild}"},
                    new {assembly = "Aline.TimeSheet.Service.Utility", 
                        oldAssemblyVersion = oldnugetPackageWasBeta ? $"1.0.0.{oldTCBuild}" : $"1.0.0.0", 
                        newAssemblyVersion = $"1.0.0.{newTCBuild}"},
                };
                
                foreach (var projectfile in repositoryFolder.EnumerateFiles("*.csproj", SearchOption.AllDirectories))
                {
                    var projectDocument = XDocument.Load(projectfile.FullName);

                    foreach (var replacement in replacements)
                    {
                        var referenceElements = projectDocument.Descendants()
                            .Where(referenceElement => referenceElement.Attribute("Include") != null
                                                       && referenceElement.Attribute("Include").Value
                                                           .Contains($"{replacement.assembly}, Version={replacement.oldAssemblyVersion}")
                                                       && referenceElement.Descendants(xpathHintPath).Count() == 1
                                                       && referenceElement.Descendants(xpathHintPath).Single().Value
                                                           .Contains($".{oldNugetPackageVersion}\\"));
                        if (referenceElements.Any())
                        {
                            Console.WriteLine(
                                $"Updating {referenceElements.Count()} references in {GetRelativePath(projectfile, repositoryFolder)} ...");
                            if (!preview)
                            {
                                projectDocument.Save(projectfile.FullName + ".orig");
                            }
                        }
                        foreach (var referenceElement in referenceElements)
                        {
                            Console.WriteLine(
                                $"\tRef:\t{referenceElement.Attribute("Include").Value,-130}{replacement.oldAssemblyVersion} => {replacement.newAssemblyVersion}");
                            referenceElement.Attribute("Include").Value = referenceElement.Attribute("Include").Value
                                .Replace(replacement.oldAssemblyVersion, replacement.newAssemblyVersion);

                            var hithPathElement = referenceElement.Descendants(xpathHintPath).Single();
                            Console.WriteLine(
                                $"\tNuget:\t{hithPathElement.Value,-130}{oldNugetPackageVersion} => {newNugetPackageVersion}");
                            hithPathElement.Value =
                                hithPathElement.Value.Replace(oldNugetPackageVersion, newNugetPackageVersion);
                        }
                    }

                    if (!preview)
                    {
                        projectDocument.Save(projectfile.FullName);
                    }
                }
            }
            Console.WriteLine(new string('=', 80));
            Console.WriteLine($"Done!");
            Console.ReadKey();
        }

        private static string GetRelativePath(FileSystemInfo fileInfo, DirectoryInfo folder)
        {
            Uri pathUri = new Uri(fileInfo.FullName);
            Uri folderUri = new Uri(folder.FullName);
            return Uri.UnescapeDataString(folderUri.MakeRelativeUri(pathUri).ToString()
                .Replace('/', Path.DirectorySeparatorChar));
        }
    }
}